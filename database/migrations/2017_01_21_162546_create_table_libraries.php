<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLibraries extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libraries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('name');
            $table->string('abbr');
            $table->string('url');
            $table->timestamps();
        });

        // Insert some stuff
        DB::table('libraries')->insert(
            array(
                'id' => '10123',
                'code' => 'ARC100',
                'name' => 'Architecture / Music Library',
                'abbr' => 'Arch Music',
                'url' => 'http://www.library.uq.edu.au/locations/architecture-music-library"'
            )
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('libraries');
    }
}
