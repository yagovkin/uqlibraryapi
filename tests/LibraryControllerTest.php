<?php

class LibraryControllerTest extends TestCase
{

    // Should return a JSON with library details where id 10123
    public function testExampleGetLibraryByIdSuccess()
    {
        $this->json('GET', 'api/library/10123');
        $this->seeJson([
            'id' => 10123,
            'code' => 'ARC100'
        ]);
    }


    // Should return "Not Found" as there is no library in the database with id=1
    public function testExampleGetLibraryByIdNotFound()
    {
        $this->json('GET', 'api/library/1');
        $this->seeJson([
            'message' => 'Not Found',
            'code' => 404,
        ]);
    }


    // Should return "Unauthorized" as token was not provided
    public function testExampleCreateLibraryUnauthorized()
    {
        $library = '{
                    "id":   3824,
                    "code": "BIO100",
                    "name": "Biological Sciences Library",
                    "abbr": "Bio Lib",
                    "url": "https://web.library.uq.edu.au/locations-hours/biological-sciences-library"
                    }';

        $this->json('POST', 'api/library', ['library' => $library]);
        $this->seeJson([
            'message' => 'Unauthorized',
            'code' => 401,
        ]);
    }


    // Should return a minimum value of an unsorted binary tree leaf (2)
    public function testExampleFindSmallestLeafSuccess()
    {
        $tree = '{
                  "root": 1,
                  "left": {
                    "root": 7,
                    "left": {
                      "root": 2
                    },
                    "right": {
                      "root": 6
                    }
                  },
                  "right": {
                    "root": 5,
                    "left": {
                      "root": 9
                    }
                  }
                }';

        $this->call('POST', 'api/findSmallestLeaf', ['tree' => $tree]);
        $this->assertEquals('2', $this->response->getContent());
    }

}