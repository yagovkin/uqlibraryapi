#Overview and implementation time
The project has been implemented using Laravel Lumen framework.
Lumen is a lightweight version of Laravel and works perfectly for small API projects like the current trial task.

The system has three routes that reflect the initial tasks, all the functionality works according to the requirements.
The only change that was made is the use of POST method instead of GET in the last task. The reason is
that 'tree' parameter could be a large JSON object with more complex structure than the sample JSON.
This may cause the system collapse in case of using GET method.
Unit test cases are limited and do not cover 100% of code, as the goal is to just illustrate familiarity with Laravel/Lumen unit testing.
Overall implementation time is 6-8 hours.
The functionality was manually tested using Postman Google Chrome extension.


#Files
Routes
/routes/web.php

Migrations
/database/migrations/2017_01_21_162546_create_table_libraries.php

Main controller
/app/Http/Controllers/LibraryController.php

Controller unit tests
/tests/LibraryControllerTest.php

Database config file
/.env


#Deployment instructions
1) Install project dependencies (requires Composer to be installed locally)
composer update

2)Update DB credentials in .env file
For example:
DB_PORT=8889
DB_DATABASE=uqlibraryapi
DB_USERNAME=root
DB_PASSWORD=root

3) Run migrations (the migration automaticaly creates 'libraries' table with a single entry id=10123)
php artisan migrate

4) To run unit tests
vendor/bin/phpunit


#API Documentation
1) /api/library/{id} GET
Takes 'id' (primary key in libraries table), returns a JSON with library details.
If library with requested id is not in the database - return not found response.
Test:
http://localhost/api/library/10123

2) /api/library POST
Takes a parameter 'library' JSON representation of a library object.
Requires an authentication token - X-VALID-USER: ${token} in the request header,
token value can be any value for a successful response.
Without auth token request returns unauthorised response.
Check if library with provided ID already exist in the database.
Check validation rules.
Saves library data in the database.
Sample JSON structure:
{
  "id":   10123,
  "code": "ARC100",
  "name": "Architecture / Music Library",
  "abbr": "Arch Music",
  "url": "http://www.library.uq.edu.au/locations/architecture-music-library"
}

3) /api/findSmallestLeaf POST
Takes a parameter 'tree' JSON representation of an unsorted binary tree and returns a minimum value of an unsorted binary tree leaf.
Leaf is a tree element which doesn't have any children.
Sample input:
{
  "root": 1,
  "left": {
    "root": 7,
    "left": {
      "root": 2
    },
    "right": {
      "root": 6
    }
  },
  "right": {
    "root": 5,
    "left": {
      "root": 9
    }
  }
}


#Optimised development environment
For continuous deployment and integration I would use AWS Bamboo.
After each successful build, if all the unit tests passed automated deployment should be scheduled.


#Optimise applications on server
Application could run in Docker container, with required dependencies injected inside the container.


#Scale applications
The system should not grow as a monolythic application that accumulates inside all the possible functionality.
Instead micro-services architecture should be implimented as a collection of APIs, each of which must be targeted
on performing logically related operations. Each API could be de deployed in its own Docker container.
Diffrent languages and frameworks could be used for each of the APIs, depends on the tasks performed by the application.
To make sure that APIs work together as expected, a series of integration tests should be executed before each deployment.