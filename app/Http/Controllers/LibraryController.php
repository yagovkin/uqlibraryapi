<?php

namespace App\Http\Controllers;

use App\Library;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

class LibraryController extends Controller
{
    // Returns a JSON with library details
    public function getById($id)
    {
        $library = Library::find($id);
        if ($library)
            return response()->json($library);
        else
            return response()->json(['message' => 'Not Found', 'code' => 404]);
    }


    // Creates a new library entry in the database
    public function createLibrary(Request $request)
    {
        // Check if request has token
        if ($request->headers->get('X-Valid-User')) {

            // Get library information from request
            $lib_str = $request->get('library');
            // Convert library json to object
            $lib_obj = json_decode($lib_str);

            // Check if library with provided ID already exist in the database
            $library = Library::find($lib_obj->id);
            if (!$library) {

                // Create validator
                $input = [
                    'id' => $lib_obj->id,
                    'code' => $lib_obj->code,
                    'name' => $lib_obj->name,
                    'abbr' => $lib_obj->abbr,
                    'url' => $lib_obj->url
                ];

                $rules = [
                    'id' => 'required|numeric|min:0',
                    'code' => 'required|library_code',
                    'name' => 'required|string',
                    'abbr' => 'required|string',
                    'url' => 'required|url'
                ];

                // Add a custom validation message for library code
                $messages = ['library_code' => 'The library code must be a 3 character, 3 number combination.'];

                // Add a custom validation for library code, should be a 3 character, 3 number combination
                Validator::extend('library_code', function ($attribute, $value, $parameters) {
                    if (preg_match('/^[a-zA-Z]{3}[0-9]{3}$/', $value))
                        return 1;
                });

                $validator = Validator::make($input, $rules, $messages);

                // Send response if validation fails
                if ($validator->fails()) {
                    return response()->json(['validation_errors' => $validator->messages()]);
                }
                // If validation passed - create new library object
                else {
                    $library = new Library;

                    $library->id = $lib_obj->id;
                    $library->code = $lib_obj->code;
                    $library->name = $lib_obj->name;
                    $library->abbr = $lib_obj->abbr;
                    $library->url = $lib_obj->url;

                    if ($library->save()) {
                        return response()->json($library);
                    } else {
                        return response()->json(['message' => 'Error occurred while creating a new library.', 'code' => 500]);
                    }
                }
            }
            else {
                return response()->json(['message' => 'Library with provided ID already exist in the database', 'code' => 422]);
            }
        } else {
            return response()->json(['message' => 'Unauthorized', 'code' => 401]);
        }
    }


    // Returns a minimum value of an unsorted binary tree leaf
    public function findSmallestLeaf(Request $request)
    {
        // Get tree information from request
        $tree_str = $request->get('tree');
        // Convert tree json to object
        $tree_obj = json_decode($tree_str);


        // Recursive function that builds an array of leaves root values
        function traverse($tree)
        {
            static $root_arr = array();

            if (!empty($tree->left))
                traverse($tree->left);
            if (!empty($tree->right))
                traverse($tree->right);
            // If leaf push root value to the array, return array
            if (empty($tree->left) && empty($tree->right))
                array_push($root_arr, $tree->root);
            return $root_arr;
        }

        $response = traverse($tree_obj);

        // Return a minimum value of an unsorted binary tree leaf
        return response()->json(min($response));

    }

}

?>